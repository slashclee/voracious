using Gee;
using Gtk;
using GLib;
using GXml;

public class Epub : Object {
	public string filename;
	public string[] spine;
	public Gdk.Pixbuf cover;
	public string title;

	public Epub(string f) {
		filename = f;
	}

	public void load() {
		info("loading epub file");
		var e = new EpubInternal(filename);
		var container_xml = e["META-INF/container.xml"];

		info("loading rootpath from container");
		var rootpath = rootpath_from_container(container_xml);
		if (rootpath == null) {
			error("couldn't find rootfile");
		}

		info("loading cover image path");
		var c = cover_image_path(e[rootpath]);
		var rootdir = Path.get_dirname(rootpath);
		if (rootdir != ".") {
			c = Path.build_filename(rootdir, c);
		}

		var m = new MemoryInputStream.from_data(e.contents[c].data, GLib.free);
		try {
			var fs_cover = new Gdk.Pixbuf.from_stream(m);
			var ratio = 120.0 / fs_cover.height;
			cover = fs_cover.scale_simple((int)(fs_cover.width * ratio), (int)(fs_cover.height * ratio), Gdk.InterpType.BILINEAR);
		} catch (GLib.Error e) {
			error(e.message);
		}

		title = title_from_root(e[rootpath]);
		spine = spine_from_root(e[rootpath], rootdir == "." ? null : rootdir);
	}

	private GXml.GDocument doc_from_xml(string xml) {
		try {
			var doc = new GDocument.from_string(xml);
			return doc;
		} catch (GLib.Error e) {
			error(e.message);
		}
	}

	private GListNamespaces ns(GDocument doc) {
		var ns = doc.namespaces;
		
		ns.add(new GNamespace(new Xml.Ns(null, "http://www.idpf.org/2007/opf", "opf")));
		ns.add(new GNamespace(new Xml.Ns(null, "http://purl.org/dc/elements/1.1/", "dc")));
		ns.add(new GNamespace(new Xml.Ns(null, "urn:oasis:names:tc:opendocument:xmlns:container", "c")));

		var size = ns.size;
		debug(@"ns size: $size");
		return ns as GListNamespaces;
	}

	private string rootpath_from_container(string xml) {
		try {
			debug("loading document from rootpath XML");
			var doc = doc_from_xml(xml);
			debug("evaluating XPath search");
			var r = doc.evaluate("string(//c:rootfile/@full-path)", ns(doc));
			debug("did that crash?");
			return r.string_value;
		} catch (GXml.XPathError e) {
			error(e.message);
		}
	}

	private string cover_image_path(string xml) {
		try {
			var doc = new GDocument.from_string(xml);
			var r = doc.evaluate("string(//opf:meta[@name='cover']/@content)", ns(doc));
			var cover_id = r.string_value;
			r = doc.evaluate(@"string(//opf:item[@id='$cover_id']/@href)", ns(doc));
			return r.string_value;
		} catch (GLib.Error e) {
			error(e.message);
		}
	}

	private string title_from_root(string xml) {
		try {
			var doc = new GDocument.from_string(xml);
			var r = doc.evaluate("string(//dc:title)", ns(doc));
			return r.string_value;
		} catch (GLib.Error e) {
			error(e.message);
		}
	}

	private string[] spine_from_root(string xml, string? rootdir) {
		string[] ret = {};
		try {
			var doc = new GDocument.from_string(xml);
			var r = doc.evaluate("//opf:spine/opf:itemref/@idref", ns(doc));
			for (int i = 0; i < r.nodeset.length; i++) {
				var id = r.nodeset.item(i).node_value;
				var nr = doc.evaluate(@"string(//opf:item[@id='$id']/@href)", ns(doc));
				if (rootdir == null) {
					ret += nr.string_value;
				} else {
					ret += Path.build_filename(rootdir, nr.string_value);
				}
			}
		} catch (GLib.Error e) {
			error(e.message);
		}

		return ret;
	}
}
